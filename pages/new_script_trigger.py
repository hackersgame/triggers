#!/usr/bin/python3
#Triggers GPL3
#Copyright (C) 2019 David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
import os
import shutil

script_path = os.path.dirname(os.path.realpath(__file__)) + "/../"


def show_add_trigger_sript(widget, Gtk, content, window):
    #remove grid
    page_stuff = content.get_children()
    header = page_stuff[0]
    mid_part = page_stuff[1]
    back_button = page_stuff[-1]
    if len(page_stuff) == 3:
        content.remove(mid_part)
        content.remove(back_button)
    header.set_markup("<big><big>Trigger via script</big></big>")

    ###########################add trigger script page##############################
    
    script_info_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=9)
    
    script_name_label = Gtk.Label()
    script_name_label.set_markup("<big><big>Name:</big></big>")
    script_name_label.set_halign(Gtk.Align.START)
    
    script_name_entry = Gtk.Entry()
    
    name_grid = Gtk.Grid(column_spacing=25)
    name_grid.attach(script_name_label,  0, 0, 1, 1)
    name_grid.attach(script_name_entry,  1, 0, 1, 1)
    ########################Script path part###########################
    script_path_separator = Gtk.Separator(orientation = Gtk.Orientation.HORIZONTAL)
    path_to_script_label = Gtk.Label()
    path_to_script_label.set_markup("\n<big><big>Path to trigger script:</big></big>")
    path_to_script_label.set_halign(Gtk.Align.START)
    
    display_path_label = Gtk.Label()
    display_path_label.set_halign(Gtk.Align.START)
    
    browse_to_file = Gtk.Button("Browse")
    browse_to_file.connect("clicked", select_dialog, Gtk, display_path_label, "Select trigger script")
    browse_to_file.set_halign(Gtk.Align.END)
    
    script_path_grid = Gtk.Grid(column_spacing=25)
    script_path_grid.attach(path_to_script_label,  0, 0, 1, 1)
    script_path_grid.attach(browse_to_file,  1, 0, 1, 1)
    script_path_grid.attach(display_path_label,  1, 1, 1, 1)
    
    ########################Return code part###########################
    top_return_separator = Gtk.Separator(orientation = Gtk.Orientation.HORIZONTAL)
    return_code_label = Gtk.Label()
    return_code_label.set_markup("\n<big><big>Trigger on return code:</big></big>")
    return_code_label.set_halign(Gtk.Align.START)

    script_return_code = Gtk.Entry()
    script_return_code.set_width_chars(1)
    script_return_code.set_text('0')
    
    return_code_grid = Gtk.Grid(column_spacing=25)
    return_code_grid.attach(return_code_label,  0, 0, 1, 1)
    return_code_grid.attach(script_return_code,  1, 0, 1, 1)
    
    ##########################Reset part############################
    top_reset_separator = Gtk.Separator(orientation = Gtk.Orientation.HORIZONTAL)
    bot_reset_separator = Gtk.Separator(orientation = Gtk.Orientation.HORIZONTAL)
    
    reset_script_label = Gtk.Label()
    reset_script_label.set_markup("<big><big>Reset trigger:</big></big>")
    reset_script_label.set_halign(Gtk.Align.START)
    
    reset_script_time_label = Gtk.Label()
    reset_script_time_label.set_markup("<big><big>After minute:</big></big>")
    reset_script_time_label.set_halign(Gtk.Align.START)
    
    reset_after_entry = Gtk.Entry()
    reset_after_entry.set_width_chars(2)
    reset_after_entry.set_text('30')
    
    reset_script_path_label = Gtk.Label()
    reset_script_path_label.set_halign(Gtk.Align.START)
    
    browse_reset_file = Gtk.Button("or via script")
    browse_reset_file.connect("clicked", select_dialog, Gtk, reset_script_path_label, "Select reset script")
    browse_reset_file.set_halign(Gtk.Align.END)
    
    reset_grid = Gtk.Grid(column_spacing=25)
    reset_grid.attach(reset_script_label,  0, 0, 1, 1)
    reset_grid.attach(reset_script_time_label,  1, 1, 1, 1)
    reset_grid.attach(reset_after_entry,  2, 1, 1, 1)
    reset_grid.attach(browse_reset_file,  2, 2, 1, 1)
    reset_grid.attach(reset_script_path_label,  2, 3, 1, 1)
    
    ##########################Add button############################
    add_button =  Gtk.Button()
    add_button_image = Gtk.Image.new_from_file( script_path + '/img/add.png')
    add_button.add(add_button_image)
    raw_data = [script_name_entry, display_path_label, reset_after_entry, script_return_code, reset_script_path_label]
    add_button.connect("clicked", add_trigger_script, Gtk, content, window, raw_data)
    add_button.set_halign(Gtk.Align.END)
    
    ########################load into box###########################
    script_info_box.pack_start(name_grid, False, False, 0)
    script_info_box.pack_start(script_path_separator, False, False, 0)
    script_info_box.pack_start(script_path_grid, False, False, 0)
    script_info_box.pack_start(top_return_separator, False, False, 0)
    script_info_box.pack_start(return_code_grid, False, False, 0)
    script_info_box.pack_start(top_reset_separator, False, False, 0)
    script_info_box.pack_start(reset_grid, False, False, 25)
    script_info_box.pack_start(bot_reset_separator, False, False, 0)
    script_info_box.pack_start(add_button, False, False, 0)


    
    
    content.pack_start(script_info_box, expand=True, fill=False, padding = 0)
    content.pack_start(back_button, expand=True, fill=False, padding = 0)
    update_window(window)
    #print(dir(content))


def select_dialog(widget, Gtk, label, msg):
    global treeView
    global selected_file
    dialog = Gtk.FileChooserDialog(msg, None, Gtk.FileChooserAction.OPEN, ("Cancel", Gtk.ResponseType.CANCEL, "Open", Gtk.ResponseType.OK))
    dialog.set_default_size(360,720)
    response = dialog.run()
    if response == Gtk.ResponseType.OK:
        label.set_markup(dialog.get_filename())
        #treeView.set_model(create_model())
        #selected_file = os.getcwd()
        print(dialog.get_filename())
    dialog.destroy()
    
def add_trigger_script(widget, Gtk, content, window, raw_trigger_data):
    global TRIGGERS_DIR
    ###########################Setup Vars#############################
    
    name          = raw_trigger_data[0].get_text() #script_name_entry
    path          = raw_trigger_data[1].get_text() #display_path_label
    reset_time    = raw_trigger_data[2].get_text() #reset_after_entry
    return_on     = raw_trigger_data[3].get_text() #script_return_code
    reset_script  = raw_trigger_data[4].get_text() #reset_script_path_label
    
    script_type = path.split(".")[-1]
    install_path = TRIGGERS_DIR + name + "." + script_type
    reset_install_path = TRIGGERS_DIR + name  + "_reset.sh"
    
    reset_script_setup = False
    ##########################Check stuff#############################
    if name == "":
        dialog = Gtk.Dialog(title="Error, Enter name.", parent=None, flags=0, buttons=("Okay", 1))
        dialog_label = Gtk.Label()
        dialog_label.set_markup("<big><big><big>Please enter a name</big></big></big>")
        dialog.get_content_area().add(dialog_label)
        dialog.get_content_area().set_size_request(360,720)
        dialog.show_all()
        response = dialog.run()
        dialog.destroy()
        return
    
    if path == "":
        dialog = Gtk.Dialog(title="Error, Enter path to script.", parent=None, flags=0, buttons=("Okay", 1))
        dialog_label = Gtk.Label()
        dialog_label.set_markup("<big><big><big>Please select a script.</big></big></big>")
        dialog.get_content_area().add(dialog_label)
        dialog.get_content_area().set_size_request(360,720)
        dialog.show_all()
        response = dialog.run()
        dialog.destroy()
        return
    if reset_script == "":
        reset_script = generate_sleep_script(reset_time, name + "_reset")
        reset_script_setup = True
    #######################Ask to link or copy########################
    dialog = Gtk.Dialog(title="Link/Copy", parent=None, flags=0, buttons=("Link", 1, "Copy", 2))
    label_txt = "<big><big>Link or copy to your scripts?</big>\n\nLinking is best if you plan to edit the script.\nCopy is best for cleanliness.</big>"
    dialog_label = Gtk.Label()
    dialog_label.set_markup(label_txt)
    dialog.get_content_area().add(dialog_label)
    dialog.get_content_area().set_size_request(360,720)
    dialog.show_all()
    response = dialog.run()
    dialog.destroy()
    print("TODO..." + str(response))
    
    #Handle link button press
    if response == 1:
        os.symlink(path, install_path)
        if not reset_script_setup:
            os.symlink(reset_script, reset_install_path)
            
    #Handle copy button press
    if response == 2:
        shutil.copy2(path, install_path)
        if not reset_script_setup:
            shutil.copy2(reset_script, reset_install_path)
    
    #update permissions
    os.chmod(install_path, 0o775)
    os.chmod(reset_script, 0o775)
    
    #return to main page
    display_main_page(widget, Gtk, content, window)
    
def generate_sleep_script(time_to_sleep, name):
    global TRIGGERS_DIR
    script = "#!/bin/bash\nsleep " + time_to_sleep + "m" + "\n"
    script_path = TRIGGERS_DIR + name + ".sh"
    with open(script_path, 'w+') as fh:
        fh.write(script)
    os.chmod(script_path, 0o775)
    return(script_path)
        
    
