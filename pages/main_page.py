#Triggers GPL3
#Copyright (C) 2019 David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
#import os

script_path = os.path.dirname(os.path.realpath(__file__)) + "/../"

def display_main_page(widget, Gtk, content, window):
    #remove grid
    page_stuff = content.get_children()
    header = page_stuff[0]
    back_button = page_stuff[-1]
    if len(page_stuff) == 3:
        mid_part = page_stuff[1]
        content.remove(mid_part)
    content.remove(back_button)
    header.set_markup("<big><big>Welcome</big></big>")
    ###########################Main page##############################
    #Setup grid on main page
    #Top Left
    add_trigger_button =  Gtk.Button()
    add_trigger_image = Gtk.Image.new_from_file( script_path + '/img/add_trigger.png')
    add_trigger_button.add(add_trigger_image)
    add_trigger_button.connect("clicked", show_trigger_select, Gtk, content, window)
    #Top Right
    add_event_button =  Gtk.Button()
    add_event_image = Gtk.Image.new_from_file( script_path + '/img/add_event.png')
    add_event_button.add(add_event_image)
    add_event_button.connect("clicked", show_add_event, Gtk, content, window)
    #Bot Left
    add_action_button =  Gtk.Button()
    add_action_image = Gtk.Image.new_from_file( script_path + '/img/add_action.png')
    add_action_button.add(add_action_image)
    add_action_button.connect("clicked", show_action_select, Gtk, content, window)
    #Bot Right
    remove_button =  Gtk.Button()
    remove_image = Gtk.Image.new_from_file( script_path + '/img/remove.png')
    remove_button.add(remove_image)
    remove_button.connect("clicked", show_remove)

    main_grid = Gtk.Grid(column_homogeneous=True,
                            column_spacing=2,
                            row_spacing=2)
    #left, top, width, height
    main_grid.attach(add_trigger_button, 0, 0, 1, 1)
    main_grid.attach(add_event_button, 1, 0, 1, 1)
    main_grid.attach(add_action_button, 0, 1, 1, 1)
    main_grid.attach(remove_button, 1, 1, 1, 1)
    
    
    content.pack_start(main_grid, expand=True, fill=False, padding = 0)
    content.pack_start(back_button, expand=True, fill=False, padding = 0)
    update_window(window)

def show_remove(widget):
    pass
