#!/usr/bin/python3

#Triggers GPL3
#Copyright (C) 2019 David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
import os

###############################Vars##############################

global installed_triggers
global installed_trigger_resets
global installed_actions
global HOME_DIR
global script_path
global TRIGGERS_DIR
global ACTIONS_DIR
global EVENT_DIR
global CONFIG_FILE
global CONFIG

installed_triggers = {}
installed_trigger_resets = {}
installed_actions = {}
AUTO_START_DESKTOP = os.path.expanduser('~') + "/.config/autostart/"
HOME_DIR = os.path.expanduser('~') + "/.triggers/"
TRIGGERS_DIR = HOME_DIR + "triggers/"
ACTIONS_DIR = HOME_DIR + "actions/"
EVENT_DIR = HOME_DIR + "events/"
CONFIG_FILE = HOME_DIR + "config.txt"
CONFIG = {}

def init():
    global installed_triggers
    global installed_trigger_resets
    global installed_actions
    global HOME_DIR
    global script_path
    global TRIGGERS_DIR
    global ACTIONS_DIR
    global EVENT_DIR
    global CONFIG_FILE
    global CONFIG
    
    script_path = os.path.dirname(os.path.realpath(__file__)) + "/../"
    
    #setup ~/.triggers/
    if not os.path.isdir(HOME_DIR):
        os.makedirs(HOME_DIR)
    
    #setup ~/.triggers/triggers/
    if not os.path.isdir(TRIGGERS_DIR):
        os.makedirs(TRIGGERS_DIR)
    
    #setup ~/.triggers/actions/
    if not os.path.isdir(ACTIONS_DIR):
        os.makedirs(ACTIONS_DIR)

    #setup ~/.triggers/conifg.txt
    if not os.path.exists(CONFIG_FILE):
        build_config()

    #setup ~/.triggers/events/
    if not os.path.exists(EVENT_DIR):
        os.makedirs(EVENT_DIR)
        
    #setup ~/.config/autostart
    if not os.path.exists(AUTO_START_DESKTOP):
        os.makedirs(AUTO_START_DESKTOP)
    
    #load installed triggers
    for trig in os.listdir(TRIGGERS_DIR):
        trigger_path = os.path.join(TRIGGERS_DIR, trig)
        trigger_name = os.path.basename(trigger_path).split(".")[0]
        if trigger_name.endswith("_reset"):
            continue
        installed_triggers[trigger_name] = trigger_path
    #load installed trigger resets
    for trig in os.listdir(TRIGGERS_DIR):
        trigger_path = os.path.join(TRIGGERS_DIR, trig)
        trigger_name = os.path.basename(trigger_path).split(".")[0]
        if not trigger_name.endswith("_reset"):
            continue
        installed_trigger_resets[trigger_name] = trigger_path
        
    #load installed actions
    for act in os.listdir(ACTIONS_DIR):
        action_path = os.path.join(ACTIONS_DIR, act)
        action_name = os.path.basename(action_path).split(".")[0]
        installed_actions[action_name] = action_path

def build_config():
    global CONFIG_FILE
    default_config = ""
    with open(CONFIG_FILE, 'w+') as fh:
        fh.write(default_config)
init()
