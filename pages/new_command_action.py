#!/usr/bin/python3
#Triggers GPL3
#Copyright (C) 2019 David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
import os
import shutil

script_path = os.path.dirname(os.path.realpath(__file__)) + "/../"


def show_add_action_command(widget, Gtk, content, window):
    #remove grid
    page_stuff = content.get_children()
    header = page_stuff[0]
    mid_part = page_stuff[1]
    back_button = page_stuff[-1]
    if len(page_stuff) == 3:
        content.remove(mid_part)
        content.remove(back_button)
    header.set_markup("<big><big>Action via command</big></big>")

    ###########################add action script page##############################
    
    script_info_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=9)
    
    script_name_label = Gtk.Label()
    script_name_label.set_markup("<big><big>Name:</big></big>")
    script_name_label.set_halign(Gtk.Align.START)
    
    script_name_entry = Gtk.Entry()
    
    name_grid = Gtk.Grid(column_spacing=25)
    name_grid.attach(script_name_label,  0, 0, 1, 1)
    name_grid.attach(script_name_entry,  1, 0, 1, 1)
    #############################command input part################################
    script_path_separator = Gtk.Separator(orientation = Gtk.Orientation.HORIZONTAL)
    command_label = Gtk.Label()
    command_label.set_markup("\n<big><big>Enter command:</big></big>")
    command_label.set_halign(Gtk.Align.START)
    
    command_entry = Gtk.Entry()
    
    command_grid = Gtk.Grid(column_spacing=25)
    command_grid.attach(command_label,  0, 0, 1, 1)
    command_grid.attach(command_entry,  1, 0, 1, 1)
    
    ##########################Add button############################
    add_button =  Gtk.Button()
    add_button_image = Gtk.Image.new_from_file( script_path + '/img/add.png')
    add_button.add(add_button_image)
    raw_data = [script_name_entry, command_entry]
    add_button.connect("clicked", add_action_command, Gtk, content, window, raw_data)
    add_button.set_halign(Gtk.Align.END)
    
    ########################load into box###########################
    script_info_box.pack_start(name_grid, False, False, 0)
    script_info_box.pack_start(script_path_separator, False, False, 0)
    script_info_box.pack_start(command_grid, False, False, 0)
    script_info_box.pack_start(add_button, False, False, 150)


    
    
    content.pack_start(script_info_box, expand=True, fill=False, padding = 0)
    content.pack_start(back_button, expand=True, fill=False, padding = 0)
    update_window(window)
    #print(dir(content))


def add_action_command(widget, Gtk, content, window, raw_action_data):
    global ACTIONS_DIR
    ###########################Setup Vars#############################
    
    name          = raw_action_data[0].get_text() #script_name_entry
    command       = raw_action_data[1].get_text() #
    
    script_type = 'sh'
    install_path = ACTIONS_DIR + name + "." + script_type
    ##########################Check stuff#############################
    if name == "":
        dialog = Gtk.Dialog(title="Error, Enter name.", parent=None, flags=0, buttons=("Okay", 1))
        dialog_label = Gtk.Label()
        dialog_label.set_markup("<big><big><big>Please enter a name</big></big></big>")
        dialog.get_content_area().add(dialog_label)
        dialog.get_content_area().set_size_request(360,720)
        dialog.show_all()
        response = dialog.run()
        dialog.destroy()
        return
    
    if command == "":
        dialog = Gtk.Dialog(title="Error, Enter command", parent=None, flags=0, buttons=("Okay", 1))
        dialog_label = Gtk.Label()
        dialog_label.set_markup("<big><big><big>Please enter a valid command.</big></big></big>")
        dialog.get_content_area().add(dialog_label)
        dialog.get_content_area().set_size_request(360,720)
        dialog.show_all()
        response = dialog.run()
        dialog.destroy()
        return
    
    #########################Setup script#############################
    script = "#!/bin/bash\n" + command + '\n'
    with open(install_path, 'w+') as fh:
        fh.write(script)

    display_main_page(widget, Gtk, content, window)
