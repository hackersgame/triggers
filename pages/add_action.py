#!/usr/bin/python3
#Triggers GPL3
#Copyright (C) 2019 David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
import os
import shutil
from gi.repository import Gdk

script_path = os.path.dirname(os.path.realpath(__file__)) + "/../"


def show_add_event(widget, Gtk, content, window):
    global installed_triggers
    global installed_actions
    
    #update triggers/actions
    init()
    #remove grid
    page_stuff = content.get_children()
    header = page_stuff[0]
    mid_part = page_stuff[1]
    back_button = page_stuff[-1]
    if len(page_stuff) == 3:
        content.remove(mid_part)
        content.remove(back_button)
    header.set_markup("<big><big>Event</big></big>")

    ###########################add trigger script page##############################
    
    event_info_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=9)
    
    event_name_label = Gtk.Label()
    event_name_label.set_markup("<big><big>Name:</big></big>")
    event_name_label.set_halign(Gtk.Align.START)
    
    event_name_entry = Gtk.Entry()
    
    name_grid = Gtk.Grid(column_spacing=25)
    name_grid.attach(event_name_label,  0, 0, 1, 1)
    name_grid.attach(event_name_entry,  1, 0, 1, 1)
    #############################Trigger select part#################################
    trigger_dropdown_separator = Gtk.Separator(orientation = Gtk.Orientation.HORIZONTAL)
    trigger_dropdown_label = Gtk.Label()
    trigger_dropdown_label.set_markup("\n<big><big>On:</big></big>")
    trigger_dropdown_label.set_halign(Gtk.Align.START)
    
    
    trigger_dropdown = Gtk.ComboBoxText()
    for t in installed_triggers.keys():
        trigger_dropdown.append_text(t)
    
    
    script_path_grid = Gtk.Grid(column_spacing=25)
    script_path_grid.attach(trigger_dropdown_label,  0, 0, 1, 1)
    script_path_grid.attach(trigger_dropdown,  1, 0, 1, 1)
    #script_path_grid.attach(display_path_label,  1, 1, 1, 1)
    
    ######################todo list part#########################
    do_separator = Gtk.Separator(orientation = Gtk.Orientation.HORIZONTAL)

    event_do_label = Gtk.Label()
    event_do_label.set_markup("<big><big>Do:</big></big>")
    event_do_label.set_halign(Gtk.Align.START)
    
    
    todo_list = Gtk.Label()
    todo_list.set_halign(Gtk.Align.START)
    
    todo_list_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=3)
    #todo_list_box.override_background_color(Gtk.StateType.NORMAL, Gdk.RGBA(1,1,1,1)) I wish this worked...
    todo_list_box.pack_start(todo_list, False, False, 0)
    
    what_todo_grid = Gtk.Grid(column_spacing=25)
    what_todo_grid.attach(event_do_label,  0, 0, 1, 1)
    what_todo_grid.attach(todo_list_box,  1, 1, 3, 1)
    
    ##########################add to TODO###########################
    include_separator = Gtk.Separator(orientation = Gtk.Orientation.HORIZONTAL)
    
    action_dropdown = Gtk.ComboBoxText()
    for a in installed_actions.keys():
        action_dropdown.append_text(a)
        
    include_action_button = Gtk.Button("include")
    include_action_button.connect("clicked", add_to_todo, action_dropdown, todo_list)
    
    action_add_grid = Gtk.Grid(column_spacing=15)
    action_add_grid.attach(action_dropdown,  0, 0, 1, 1)
    action_add_grid.attach(include_action_button,  1, 0, 3, 3)

    ##########################Add button############################
    add_button =  Gtk.Button()
    add_button_image = Gtk.Image.new_from_file( script_path + '/img/add.png')
    add_button.add(add_button_image)
    raw_data = [event_name_entry, trigger_dropdown, todo_list]
    add_button.connect("clicked", add_event, Gtk, content, window, raw_data)
    add_button.set_halign(Gtk.Align.END)
    
    ########################load into box###########################
    event_info_box.pack_start(name_grid, False, False, 0)
    event_info_box.pack_start(trigger_dropdown_separator, False, False, 0)
    event_info_box.pack_start(script_path_grid, False, False, 0)
    event_info_box.pack_start(do_separator, False, False, 0)
    event_info_box.pack_start(what_todo_grid, False, False, 0)
    event_info_box.pack_start(include_separator, False, False, 0)
    event_info_box.pack_start(action_add_grid, False, False, 0)
    #event_info_box.pack_start(scroll_todo_list, False, False, 25)
    #event_info_box.pack_start(bot_reset_separator, False, False, 0)
    event_info_box.pack_start(add_button, False, False, 0)

    
    content.pack_start(event_info_box, expand=True, fill=False, padding = 0)
    content.pack_start(back_button, expand=True, fill=False, padding = 0)
    update_window(window)
    #print(dir(content))

def setup_autorun(script_path, name):
    global AUTO_START_DESKTOP
    
    autorun_script_path = AUTO_START_DESKTOP + "trigger_" + name + ".desktop"
    desktop_file = "[Desktop Entry]\n"
    desktop_file = desktop_file + "Name=" + name + '\n'
    desktop_file = desktop_file + "Exec='" + script_path + "'\n"
    desktop_file = desktop_file + "Terminal=false\nType=Application\n"
    desktop_file = desktop_file + "X-GNOME-Autostart-enabled=true" + '\n'
    
    print(autorun_script_path)
    #print(desktop_file)
    with open(autorun_script_path, "w+") as fh:
        fh.write(desktop_file)
    os.chmod(autorun_script_path, 0o775)
    
def write_bash_script(name, trigger, todo_list):
    global installed_triggers
    global installed_actions
    global installed_trigger_resets
    
    global EVENT_DIR
    #####################Setup Vars needed for script#################
    trigger_path = installed_triggers[trigger]
    trigger_reset_path = installed_trigger_resets[trigger + "_reset"]
    script_name = EVENT_DIR + name + ".sh"
    commands_to_run = ""
    for command in todo_list:
        commands_to_run = "    " + commands_to_run + "'" + installed_actions[command] + "' &\n"
    
    ##########################Setup script############################
    bash_script = "#!/bin/bash\nwhile :\ndo\n  '" + trigger_path + "'"#commands_to_run
    #save return code:
    bash_script = bash_script + "\n  return_val=$?\n"
    bash_script = bash_script + "  if [ \"$return_val\" == 0 ]\n  then\n    echo 'Trigger'\n" + commands_to_run + "  else\n    echo \"Something went wrong: $return_val\";\n  fi"
    #reset part:
    bash_script = bash_script + "\n  echo 'Resetting...'\n  " + trigger_reset_path + "\ndone"
    #print(bash_script)
    ##########################Write script############################
    with open(script_name, 'w+') as fh:
        fh.write(bash_script)
    os.chmod(script_name, 0o775)
    return script_name


def add_event(widget, Gtk, content, window, raw_event_data):
    global TRIGGERS_DIR
    ###########################Setup Vars#############################
    
    name      = raw_event_data[0].get_text()       #event_name_entry
    trigger   = raw_event_data[1].get_active_text()#trigger_dropdown
    todo_list = raw_event_data[2].get_text()       #todo_list
    todo_list = todo_list.strip().split('\n')    
    
    ##########################Check stuff#############################
    if name == "":
        dialog = Gtk.Dialog(title="Error, Enter name.", parent=None, flags=0, buttons=("Okay", 1))
        dialog_label = Gtk.Label()
        dialog_label.set_markup("<big><big><big>Please enter a name</big></big></big>")
        dialog.get_content_area().add(dialog_label)
        dialog.get_content_area().set_size_request(360,720)
        dialog.show_all()
        response = dialog.run()
        dialog.destroy()
        return
    
    if trigger == "":
        dialog = Gtk.Dialog(title="Error, Select trigger.", parent=None, flags=0, buttons=("Okay", 1))
        dialog_label = Gtk.Label()
        dialog_label.set_markup("<big><big><big>Please select a trigger to use.</big></big></big>")
        dialog.get_content_area().add(dialog_label)
        dialog.get_content_area().set_size_request(360,720)
        dialog.show_all()
        response = dialog.run()
        dialog.destroy()
        return
    if todo_list == []:
        dialog = Gtk.Dialog(title="Error, empty action list.", parent=None, flags=0, buttons=("Okay", 1))
        dialog_label = Gtk.Label()
        dialog_label.set_markup("<big><big><big>Please include some actions.</big></big></big>")
        dialog.get_content_area().add(dialog_label)
        dialog.get_content_area().set_size_request(360,720)
        dialog.show_all()
        response = dialog.run()
        dialog.destroy()
        return
    ###########################Write stuff############################
    new_script = write_bash_script(name, trigger, todo_list)
    setup_autorun(new_script, name)
    
    ########################Back to main page#########################
    display_main_page(widget, Gtk, content, window)
    
        
def add_to_todo(widget, action_dropdown, todo_list):
    ###########################Setup Vars#############################
    new_item = action_dropdown.get_active_text()
    todo_txt = todo_list.get_text()
    #########################Write new data###########################
    todo_list.set_text(todo_txt + "\n" + new_item)
