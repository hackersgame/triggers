#!/usr/bin/python3
#Triggers GPL3
#Copyright (C) 2019 David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
import os

script_path = os.path.dirname(os.path.realpath(__file__)) + "/../"


def show_action_select(widget, Gtk, content, window):
    #remove grid
    page_stuff = content.get_children()
    header = page_stuff[0]
    mid_part = page_stuff[1]
    back_button = page_stuff[-1]
    if len(page_stuff) == 3:
        content.remove(mid_part)
        content.remove(back_button)
    header.set_markup("<big><big>Action</big></big>")
    ###########################Action type page##############################
    #Setup grid on main page
    #Top Left
    add_action_script_button =  Gtk.Button()
    add_action_script_image = Gtk.Image.new_from_file( script_path + '/img/add_action_command.png')
    add_action_script_button.add(add_action_script_image)
    add_action_script_button.connect("clicked", show_add_action_command, Gtk, content, window)
    #Top Right
    add_action_loc_button =  Gtk.Button()
    add_action_loc_image = Gtk.Image.new_from_file( script_path + '/img/add_action_script.png')
    add_action_loc_button.add(add_action_loc_image)
    add_action_loc_button.connect("clicked", show_add_action_sript, Gtk, content, window)
    #Bot Left
    add_action_time_button =  Gtk.Button()
    add_action_time_image = Gtk.Image.new_from_file( script_path + '/img/add_action_simple.png')
    add_action_time_button.add(add_action_time_image)
    #add_action_time_button.connect("clicked", show_add_action, content, window)
    #Bot Right
    remove_button =  Gtk.Button()
    remove_image = Gtk.Image.new_from_file( script_path + '/img/remove.png')
    remove_button.add(remove_image)
    remove_button.connect("clicked", show_remove)

    action_select_grid = Gtk.Grid(column_homogeneous=True,
                            column_spacing=2,
                            row_spacing=2)
    #left, top, width, height
    action_select_grid.attach(add_action_script_button, 0, 0, 1, 1)
    action_select_grid.attach(add_action_loc_button, 1, 0, 1, 1)
    action_select_grid.attach(add_action_time_button, 0, 1, 1, 1)
    action_select_grid.attach(remove_button, 1, 1, 1, 1)
    
    content.pack_start(action_select_grid, expand=True, fill=False, padding = 0)
    content.pack_start(back_button, expand=True, fill=False, padding = 0)
    update_window(window)
    #print(dir(content))
