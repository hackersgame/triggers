#!/usr/bin/python3
#Triggers GPL3
#Copyright (C) 2019 David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
import os
import shutil

script_path = os.path.dirname(os.path.realpath(__file__)) + "/../"


def show_add_action_sript(widget, Gtk, content, window):
    #remove grid
    page_stuff = content.get_children()
    header = page_stuff[0]
    mid_part = page_stuff[1]
    back_button = page_stuff[-1]
    if len(page_stuff) == 3:
        content.remove(mid_part)
        content.remove(back_button)
    header.set_markup("<big><big>Action via script</big></big>")

    ###########################add action script page##############################
    
    script_info_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=9)
    
    script_name_label = Gtk.Label()
    script_name_label.set_markup("<big><big>Name:</big></big>")
    script_name_label.set_halign(Gtk.Align.START)
    
    script_name_entry = Gtk.Entry()
    
    name_grid = Gtk.Grid(column_spacing=25)
    name_grid.attach(script_name_label,  0, 0, 1, 1)
    name_grid.attach(script_name_entry,  1, 0, 1, 1)
    ########################Script path part###########################
    script_path_separator = Gtk.Separator(orientation = Gtk.Orientation.HORIZONTAL)
    path_to_script_label = Gtk.Label()
    path_to_script_label.set_markup("\n<big><big>Path to action script:</big></big>")
    path_to_script_label.set_halign(Gtk.Align.START)
    
    display_path_label = Gtk.Label()
    display_path_label.set_halign(Gtk.Align.START)
    
    browse_to_file = Gtk.Button("Browse")
    browse_to_file.connect("clicked", select_dialog, Gtk, display_path_label, "Select action script")
    browse_to_file.set_halign(Gtk.Align.END)
    
    script_path_grid = Gtk.Grid(column_spacing=25)
    script_path_grid.attach(path_to_script_label,  0, 0, 1, 1)
    script_path_grid.attach(browse_to_file,  1, 0, 1, 1)
    script_path_grid.attach(display_path_label,  1, 1, 1, 1)
    
    
    ##########################Add button############################
    add_button =  Gtk.Button()
    add_button_image = Gtk.Image.new_from_file( script_path + '/img/add.png')
    add_button.add(add_button_image)
    raw_data = [script_name_entry, display_path_label]
    add_button.connect("clicked", add_action_script, Gtk, content, window, raw_data)
    add_button.set_halign(Gtk.Align.END)
    
    ########################load into box###########################
    script_info_box.pack_start(name_grid, False, False, 0)
    script_info_box.pack_start(script_path_separator, False, False, 0)
    script_info_box.pack_start(script_path_grid, False, False, 0)
    script_info_box.pack_start(add_button, False, False, 150)


    
    
    content.pack_start(script_info_box, expand=True, fill=False, padding = 0)
    content.pack_start(back_button, expand=True, fill=False, padding = 0)
    update_window(window)
    #print(dir(content))


def add_action_script(widget, Gtk, content, window, raw_action_data):
    global ACTIONS_DIR
    ###########################Setup Vars#############################
    
    name          = raw_action_data[0].get_text() #script_name_entry
    path          = raw_action_data[1].get_text() #display_path_label
    
    script_type = path.split(".")[-1]
    install_path = ACTIONS_DIR + name + "." + script_type
    
    ##########################Check stuff#############################
    if name == "":
        dialog = Gtk.Dialog(title="Error, Enter name.", parent=None, flags=0, buttons=("Okay", 1))
        dialog_label = Gtk.Label()
        dialog_label.set_markup("<big><big><big>Please enter a name</big></big></big>")
        dialog.get_content_area().add(dialog_label)
        dialog.get_content_area().set_size_request(360,720)
        dialog.show_all()
        response = dialog.run()
        dialog.destroy()
        return
    
    if path == "":
        dialog = Gtk.Dialog(title="Error, Enter path to script.", parent=None, flags=0, buttons=("Okay", 1))
        dialog_label = Gtk.Label()
        dialog_label.set_markup("<big><big><big>Please select a script.</big></big></big>")
        dialog.get_content_area().add(dialog_label)
        dialog.get_content_area().set_size_request(360,720)
        dialog.show_all()
        response = dialog.run()
        dialog.destroy()
        return

    #######################Ask to link or copy########################
    dialog = Gtk.Dialog(title="Link/Copy", parent=None, flags=0, buttons=("Link", 1, "Copy", 2))
    label_txt = "<big><big>Link or copy to your scripts?</big>\n\nLinking is best if you plan to edit the script.\nCopy is best for cleanliness.</big>"
    dialog_label = Gtk.Label()
    dialog_label.set_markup(label_txt)
    dialog.get_content_area().add(dialog_label)
    dialog.get_content_area().set_size_request(360,720)
    dialog.show_all()
    response = dialog.run()
    dialog.destroy()
    print("TODO..." + str(response))
    
    #Handle link button press
    if response == 1:
        os.symlink(path, install_path)
            
    #Handle copy button press
    if response == 2:
        shutil.copy2(path, install_path)
    
    display_main_page(widget, Gtk, content, window)
    

        
    
