if [ -z "$1" ]
then
    exit 0
fi

sleep $1
exit $?
